// main.c
//
// Author: Bjorn Melin
// Date: 4/7/2020


#ifdef VIRTUAL_SERIAL
#include <VirtualSerial.h>
#else
#warning VirtualSerial not defined, USB IO may not work
#define SetupHardware();
#define USB_Mainloop_Handler();
#endif

#include "common.h"
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/io.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "motor.h"
#include "leds.h"
#include "timers.h"
#include "analog.h"
#include "tasks.h"



/****************************************************************************
   ALL INITIALIZATION
****************************************************************************/
/**
 * Initializes the system
 */
void initialize_system(void) {
	SetupHardware();        // This sets up the USB hardware and stdio
	// initalize green and yellow leds
	initialize_led(GREEN);
	initialize_led(YELLOW);
	setup_ms_timer();       // initialize 1 ms timer
	// The "sanity check" which flashes leds in a pattern upon board reset
	light_show();
	// initialize motors
	setupMotor2();          // setup the motor
	setupEncoder();         // setup the encoder
	motorForward();
	initialize_tasks();     // initialize the tasks
	// motor_sanity_check();   // sanity check to ensure that the motor is working
	initialize_pot();       // initialize ADC for potentiometer
}



/****************************************************************************
   MAIN
****************************************************************************/

/**
 * Main Function
 */ 
int main(void) {
  	USBCON = 0;             // This prevents the need to reset after flashing
	initialize_system();    // call the initialization function

	// set the reference postion of the encoder, changed this to test various values
	set_ref_delta(360);	
	sei();                  // set global interrupt enable - needed for USB

	// delay 3 seconds before starting to see initial encoder values in LUFA
	_delay_ms(3000);		


	//****** CYCLIC CONTROL LOOP ******//
	// Main while loop
	while(1) {

		// call the server function for the task scheduler
		server();

	} // end while()
} // end main()
