// tasks.h
//
// Author: Bjorn Melin
// Date: 4/7/2020

#ifndef TASKS_H_
#define TASKS_H_

#include "common.h"
#include <util/delay.h>
#include <inttypes.h>


/**
 * Struct used for Tasks
 */
typedef struct task {
	uint8_t ready;
	uint32_t releaseTime;
	uint32_t period;
} TASK_STRUCT;


/**
 * Define our tasks using the TASK_STRUCT
 */
extern volatile TASK_STRUCT pd_task;			// pd control task
extern volatile TASK_STRUCT pot_task;			// potentiometer task
extern volatile TASK_STRUCT trajectory_task;	// trajectory task
extern volatile TASK_STRUCT ui_task;			// UI task


/**
 * Initializes all tasks and sets their values
 */
void initialize_tasks();

/**
 * Task Functions
 */
void ui_task_fn(void);

void pot_task_fn(void);

void pd_control(void);

void trajectory_task_fn(void);

/**
 * Task Helper Functions
 */
void set_ref_delta( int ref_degs);

uint32_t get_ms_ticks();

#endif