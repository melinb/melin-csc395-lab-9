// timer.c
//
// Author: Bjorn Melin
// Date: 4/7/2020

#include "timers.h"
#include <avr/interrupt.h>
#include "tasks.h"

// millisecond timer for the ISR
volatile uint64_t ms_ticks = 0;

/**
 * Function which initializes Timer 0
 */
void setup_ms_timer(void) {
	// set timer control register part B to use timer 0
	// timer 0 is an 8-bit timer
	// Sets WGM0 to use CTC mode (hence the 01) 
	TCCR0A |= (1<<WGM01);

	// set timer counter control register part B to use timer 0
	// clock select 0 or 1 bits (uses 64 bits)
	TCCR0B |= (1<<CS00) | (1<<CS01);

	// timer 0 channel A
	// (16,000,000 ticks / 1 sec) * (1 cnt / 64 ticks) * (1 int / 250 cnt) = 1000int/sec
	// interrupt fires once every 1ms and increments ms_ticks e/ time
	OCR0A = 250;

	// Enables the interrupt OCIE0A
	TIMSK0 |= (1<<OCIE0A);
}


/**
 * ISR for the ms_ticks timer in Timer 0.
 * Each time the ISR fires, the ms_ticks timer is incremented by 1.
 * Also the ISR for the scheduler, acts as the Server for the scheduler.
 */
ISR(TIMER0_COMPA_vect){
    ms_ticks = ms_ticks + 1;	// increment timer

	/**
	 * Schedule tasks
	 */
	if (ms_ticks >= pd_task.releaseTime) {
		pd_task.ready = 1;
		pd_task.releaseTime = ms_ticks + pd_task.period;
	}
	else if (ms_ticks >= pot_task.releaseTime) {
		pot_task.ready = 1;
		pot_task.releaseTime = ms_ticks + pot_task.period;
	}
	else if (ms_ticks >= trajectory_task.releaseTime) {
		trajectory_task.ready = 1;
		trajectory_task.releaseTime = ms_ticks + trajectory_task.period;
	}
	else if (ms_ticks >= ui_task.releaseTime) {
		ui_task.ready = 1;
		ui_task.releaseTime = ms_ticks + ui_task.period;
	}
}


/**
 * Server for the Scheduler. Called in the cyclic control loop in 
 * main.c to run tasks.
 */
void server(void) {
	if (pd_task.ready) {
		pd_control();
		pd_task.ready = 0;
	}
	// else if (pot_task.ready) {
	// 	pot_task_fn();
	// 	pot_task.ready = 0;
	// }
	// else if (trajectory_task.ready) {
	// 	trajectory_task_fn();
	// 	trajectory_task.ready = 0;
	// }
	// else if (ui_task.ready) {
	// 	ui_task_fn();
	// 	ui_task.ready = 0;
	// }
}