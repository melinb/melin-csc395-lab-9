// tasks.c
//
// Author: Bjorn Melin
// Date: 4/7/2020


#ifdef VIRTUAL_SERIAL
#include <VirtualSerial.h>
#else
#warning VirtualSerial not defined, USB IO may not work
#define SetupHardware();
#define USB_Mainloop_Handler();
#endif

#include "tasks.h"
#include "motor.h"
#include "timers.h"
#include "analog.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


/**
 * Define our tasks using the TASK_STRUCT
 */
volatile TASK_STRUCT pd_task;                       // pd control task
volatile TASK_STRUCT pot_task;                      // potentiometer task
volatile TASK_STRUCT trajectory_task;               // trajectory task
volatile TASK_STRUCT ui_task;                       // UI task



/******************************************
 ******** GLOBAL TASK FN VARIABLES ********
 ******************************************/

volatile int32_t ref_pos = 0;           // reference position of the motor
volatile int32_t last_error = 0;        // stores error of last run of pd control
volatile int32_t last_time = 0;         // stores ms time of last run of pd control
volatile int32_t kP = 1;                // initialize kD for pd control gains   
volatile float kD = 0;                  // initialize kD for pd control gains
#define conversion_rate 2249/360        // conversion from degrees to encoder counts



/****************************************************************************
   ALL INITIALIZATION
****************************************************************************/

/**
 * Initializes all tasks and sets their values
 */
void initialize_tasks() {
    pd_task = (TASK_STRUCT) {0, 0, 50};             // pd control task
    pot_task = (TASK_STRUCT) {0, 0, 100};           // potentiometer task
    trajectory_task = (TASK_STRUCT) {0, 0, 500};    // trajectory task
    ui_task = (TASK_STRUCT) {0, 0, 5000};           // UI task
}



/****************************************************************************
   Task Functions & Helper Functions 
   (PD Control, Potentiometer, Trajectory, & UI)
****************************************************************************/

/**
 * Set Reference Position Function - Sets the reference position of the 
 * encoder to the number of degrees passed in as input by converting the 
 * input from degrees to encoder units.
 * 
 * Param: ref_degs - int denoting what to set the ref pos to in degrees.
 */
void set_ref_delta(int ref_degs) {
	USB_Mainloop_Handler();                 // Handles USB communication
    // convert ref degrees into encoder units and set to ref position
	ref_pos += (ref_degs * (2249/360.0));   
    // Below line was used for debugging
	// printf("ref pos: %d\r\nref degs: %d\r\n", (int) ref_pos, ref_degs);
};


/**
 * Function used to start the ms_ticks timer.  Used in 
 * main to time the execution of various tasks.
 */ 
uint32_t get_ms_ticks() {
	cli();
	uint32_t ticks = ms_ticks;	// store curr value of ms_ticks in ticks variable
	sei();						// set global interrupt enable
	return ticks;				// return the current clock time
}


/**
 * PD Control Function - Motor positional control system 
 */
void pd_control() {
	// T = kP * P + kD * D;
	int32_t P = ref_pos - global_counts_m2;         // calculate the error
	int T = kP * P;                                 // kP is gain, using kP to modify error measure
	int dE = P - last_error;                		// change in error
	int D = dE / (get_ms_ticks() - last_time);	    // change in error / time transpired
	T = T + (kD * D);                               // set torque

	/** 
	 * If T is negative, set the motor to move backwards and set T to the abs value of T.
	 * Or else if T is positive, set the motor to move forwards
	 */
	if (T < 0) {
		motorBackward();        // set the motor to move backwards
		T = abs(T);             // set T to abs value of T so it's not negative
	}
	else {
		motorForward();         // set the motor to move forwards
	}

    // Make sure T is not greater than the TOP value
    if (T > 3800) {
        // if T is gt TOP, set its value close to the TOP value but not quite
        T = 3800;
    }

	OCR1B = T;                                  // set the speed of the motor to the curr torque value
	last_error = P;                             // set the global last error to the current error
	last_time = get_ms_ticks();                 // set the global last time to the current time
	USB_Mainloop_Handler();                     // Handles USB communication

	// Print off motor positional information to LUFA. In lab 10, I will instead print
    // this information using the UI task similar to as we saw in Lab
	printf("Encoder Count: %d\n\rADC Value: %d\n\rT: %d\n\rError: %d\n\r\n\r", 
		global_counts_m2, adc_read(9), T, (int) P);

	// the line below was used for debugging purposes
	// printf("ref position: %d\n\rconv rate: %d\n\r", (int) ref_pos, conversion_rate);
}



/****************************************************************************
   Task Functions To Be Implemented For Lab 10
****************************************************************************/


/**
 * User Interface (UI) Task Function - Polls for a key press by a user every 1000ms
 * Using key presses, a user can:
 * 		Key R/r : Set the reference position in degrees. Value is RELATIVE 
 * 			  	  to current position.
 * 		Key Z/z: Zero the encoder marking the current position as 0 degrees.
 * 		Key V/v: View the current values Kd, Kp, Vm, Pr, Pm, and T
 * 		Key S/s: Send saved data
 */
void ui_task_fn(void) {
	char c;		// declare char c, which is the key pressed
	// int new_ref_pos;

	printf("Press 'R/r' key to set the reference position in degrees,\r\n"
			"Press 'Z/z' key to zero the encoder,\r\n"
			"Press 'V/v' key to view the current states of Kd, Kp, Vm, Pr, Pm, and T,\r\n"
			"Press 'S/s' keys to send saved data.\r\n\r\n");

	// User has pressed a button on the keyboard
	if ((c = fgetc(stdin)) != EOF) {
		// Display the key that the user pressed
		printf("\r\nYou pressed key:\t%c\r\n", c);

		// On press of the 'R/r' key, set the reference position in degrees relative 
		// to the current position by letting the user enter in the # of degrees 
		if (c == 'r' || c == 'R') {
			// printf("Enter the new reference postion in degrees (i.e. 360): ");
			// scanf("%d\r\n", &new_ref_pos);		// read ref pos entered by user
			// set_ref_delta(new_ref_pos);			// set ref pos to the pos entered
			// printf("Reference position has been set to: %d\n\r", new_ref_pos);
		}
		// On press of the 'Z/z' key, zero the encoder marking the current 
		// position as 0 degrees
		else if (c == 'z' || c == 'Z') {
			global_counts_m2 = 0;		// zero the encoder
		}
		// On press of the 'V/v key, displays the current states of 
		// Kd, Kp, Vm, Pr, Pm, and T
		else if (c == 'v' || c == 'V') {
			// printf("kD = %d,\tkP = %d,\tvM = %d,\tPr = %d,\tPm = %d,\tT = %d\r\n",
			// 	(int) kD, (int) kP, (int) vM, (int) Pr, (int) Pm, (int) T)
		}
		// On press of the 'S/s' key, send saved data
		else if (c == 's' || c == 'S') {
			
		}
		// User entered an invalid key, print a message saying nothing happened
		else {
			printf("Invalid key entry, nothing happened. Enter a valid key!\n\r\n\r");
		}
	}	
}


/**
 * Potentiometer Task Function
 */
void pot_task_fn(void) {
	// kP = 20 / 1023 * adc_read(9);       // set kP using the potentiometer
}


/**
 * Trajectory Task Function
 */
void trajectory_task_fn(void) {

}

