// timer.h
//
// Author: Bjorn Melin
// Date: 4/7/2020

#ifndef TIMERS_H_
#define TIMERS_H_

#include "common.h"
#include <util/delay.h>
#include <inttypes.h>

// millisecond timer for the ISR
extern volatile uint64_t ms_ticks;

// Function which initializes Timer 0
void setup_ms_timer(void);

// Server for the Task Scheduler
void server();

#endif