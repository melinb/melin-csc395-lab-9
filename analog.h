// analog.h
//
// Author: Bjorn Melin, Pololu
// Date: 4/7/2020

#ifndef ANALOG_H_
#define ANALOG_H_

#include "common.h"

void initialize_pot();
// initialize adc
void adc_init();
uint16_t adc_read(uint8_t channel);

#endif
